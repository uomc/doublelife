package net.uomc.plugin.doublelife;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.google.gson.JsonArray;

// A list of "linked doubles" not a linked-list of doubles
public class LinkedDoubleList {
    private Set<LinkedDouble> doubles;
    public LinkedDoubleList() {
            doubles = new HashSet<>();
    }
    public UUID getDouble(UUID uuid) {
        for (LinkedDouble d : doubles)
            if (d.has(uuid))
                return d.get(uuid);

        return null;
    }

    public boolean hasDouble(UUID uuid) {
        for (LinkedDouble d : doubles)
            if (d.has(uuid))
                return true;

        return false;
    }

    public boolean unregisterDouble(UUID uuid1) {
        if (!hasDouble(uuid1))
            return false;

        boolean result = doubles.removeIf(d -> d.has(uuid1));
        return result;
    }

    public boolean registerDouble(UUID uuid1, UUID uuid2) {
        if (uuid1 == uuid2)
            return false;

        if (hasDouble(uuid1) || hasDouble(uuid2))
            return false;

        LinkedDouble dbl = new LinkedDouble(uuid1, uuid2);
        doubles.add(dbl);
        return true;
    }

    public Set<LinkedDouble> getAll() {
        return doubles;
    }

    public void clear() {
        doubles.clear();
    }

    public void fromJson(JsonArray doublesJSON) {
        doublesJSON.forEach(o -> {
            if (!o.isJsonArray())
                return;

            JsonArray array = o.getAsJsonArray();
            if (array.size() < 2)
                return;

            String p1 = array.get(0).getAsString();
            String p2 = array.get(1).getAsString();

            registerDouble(
                UUID.fromString(p1),
                UUID.fromString(p2)
            );
        });
    }

    public JsonArray toJson() {
        JsonArray doublesArray = new JsonArray();
        for (LinkedDouble d : getAll()) {
            JsonArray doubleArray = new JsonArray();
            doubleArray.add(d.getUuid1().toString());
            doubleArray.add(d.getUuid2().toString());

            doublesArray.add(doubleArray);
        }
        return doublesArray;
    }
}
