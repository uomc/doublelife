package net.uomc.plugin.doublelife;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Husk;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.lishid.openinv.IOpenInv;

import net.uomc.plugin.doublelife.commands.LinkCommand;
import net.uomc.plugin.doublelife.commands.ListLinksCommand;
import net.uomc.plugin.doublelife.commands.UnLinkCommand;
import net.uomc.plugin.doublelife.uhc.UHCManager;

public class DoubleLifePlugin extends JavaPlugin implements Listener {

    private LinkedDoubleList doubles;
    private LinkedDoubleList husks;
    private File doublesFile;
    private IOpenInv openInv;
    private UHCManager uhc;

    public DoubleLifePlugin () {
        doubles = new LinkedDoubleList();
        husks = new LinkedDoubleList();
        doublesFile = new File(getDataFolder(), "doubles.json");

        openInv = (IOpenInv) Bukkit.getPluginManager().getPlugin("OpenInv");
    }

    public Player getPlayerDouble(Player player) {
        return Bukkit.getPlayer(doubles.getDouble(player.getUniqueId()));
    }

    public LivingEntity getEntityDouble(LivingEntity player) {
        UUID currentPlayerUuid = player.getUniqueId();
        if (player instanceof Husk) {
            currentPlayerUuid = husks.getDouble(player.getUniqueId());
            if (currentPlayerUuid == null)
                return null;
        }

        UUID doubledPlayerUuid = doubles.getDouble(currentPlayerUuid);
        if (doubledPlayerUuid == null)
            return null;

        Player doubledPlayer = Bukkit.getPlayer(doubledPlayerUuid);
        if (doubledPlayer != null)
            return doubledPlayer;

        UUID doubledHusk = husks.getDouble(doubledPlayerUuid);
        if (doubledHusk == null)
            return null;

        return (LivingEntity) Bukkit.getEntity(doubledHusk);
    }

    public OfflinePlayer getOfflineLinkedDouble(Player player) {
        return Bukkit.getOfflinePlayer(doubles.getDouble(player.getUniqueId()));
    }
    public UUID getDouble(UUID player) {
        return doubles.getDouble(player);
    }

    public boolean hasDouble(LivingEntity player) {
        return doubles.hasDouble(player.getUniqueId()) || husks.hasDouble(player.getUniqueId());
    }

    public boolean registerDouble(Player player1, Player player2) {
        return doubles.registerDouble(player1.getUniqueId(), player2.getUniqueId());
    }

    public boolean unregisterDouble(Player uuid1) {
        return doubles.unregisterDouble(uuid1.getUniqueId());
    }

    private void loadDoubles() throws FileNotFoundException {
        if (!doublesFile.exists())
            return;

        FileReader reader = new FileReader(doublesFile);
        JsonReader jsonReader = new JsonReader(reader);
        JsonElement tree = JsonParser.parseReader(jsonReader);
        JsonObject doublesJSON = tree.getAsJsonObject();

        if (!doublesJSON.has("doubles"))
            return;

        doubles.fromJson(doublesJSON.get("doubles").getAsJsonArray());

        if (!doublesJSON.has("husks"))
            return;

        husks.fromJson(doublesJSON.get("husks").getAsJsonArray());

    }

    private void saveDoubles() throws IOException {
        JsonObject doublesJSON = new JsonObject();

        JsonArray doublesArray = doubles.toJson();
        doublesJSON.add("doubles", doublesArray);

        JsonArray husksArray = husks.toJson();
        doublesJSON.add("husks", husksArray);

        doublesFile.getParentFile().mkdirs();
        FileWriter fw = new FileWriter(doublesFile);
        try {
            fw.write(doublesJSON.toString());
        } finally {
                fw.flush();
                fw.close();
        }
    }

	@Override
    public void onEnable() {
        getLogger().info("enabling plugin " +  this.getName());

        uhc = new UHCManager(this);

        try {
            loadDoubles();
            uhc.loadAll();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        getCommand("linkdouble").setExecutor(new LinkCommand(this));
        getCommand("unlinkdouble").setExecutor(new UnLinkCommand(this));
        getCommand("listdoubles").setExecutor(new ListLinksCommand(this));

        getServer().getPluginManager().registerEvents(new DoubleLifeListener(this), this);

    }

    @Override
    public void onDisable() {
        getLogger().info("disabling plugin " +  this.getName());
        saveAll();
    }

    public void saveAll() {
        try {
            saveDoubles();
            if (uhc != null)
                uhc.saveAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LivingEntity createHuskDouble(Player player) {
       Husk zombie = (Husk) player.getWorld().spawnEntity(player.getLocation(), EntityType.HUSK);
        zombie.setAI(true);
        zombie.setPersistent(true);
        zombie.setSilent(true);
        zombie.setCustomName(player.getDisplayName());
        zombie.setCustomNameVisible(true);
        zombie.getEquipment().clear();
        zombie.getEquipment().setArmorContents(player.getInventory().getArmorContents());
        zombie.getEquipment().setItemInMainHand(player.getInventory().getItemInMainHand());
        zombie.getEquipment().setItemInOffHand(player.getInventory().getItemInOffHand());
        zombie.addPotionEffects(player.getActivePotionEffects());
        zombie.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
        zombie.setFireTicks(player.getFireTicks());
        zombie.setRemainingAir(player.getRemainingAir());
        zombie.setHealth(player.getHealth());
        zombie.setAbsorptionAmount(player.getAbsorptionAmount());
        zombie.setAdult();

        husks.registerDouble(player.getUniqueId(), zombie.getUniqueId());
        return zombie;
    }

    public boolean hasHusk(Player player) {
        return husks.hasDouble(player.getUniqueId());
    }

    public boolean hasHusk(LivingEntity player) {
        return husks.hasDouble(player.getUniqueId());
    }


    public LivingEntity getHusk(LivingEntity player) {
        return (LivingEntity) Bukkit.getEntity(husks.getDouble(player.getUniqueId()));
    }

    public UUID getHuskPlayerId(LivingEntity husk) {
        return husks.getDouble(husk.getUniqueId());
    }

    public void replaceHusk(Player player) {
        Entity entity = getHusk(player);

        // assume that The "player" has died:
        if (entity == null) {
            // if we have openinv, then we've probably already dropped their inventory
            if (getOpenInv() != null)
                player.getInventory().clear();
            player.setHealth(0);
        }

        husks.unregisterDouble(player.getUniqueId());

        if (!(entity instanceof Husk))
            return; // something went very wrong

        Husk zombie = (Husk) entity;

        player.setHealth(zombie.getHealth());
        player.setAbsorptionAmount(zombie.getAbsorptionAmount());
        player.getEquipment().setArmorContents(zombie.getEquipment().getArmorContents());
        player.getEquipment().setItemInMainHand(zombie.getEquipment().getItemInMainHand());
        player.getEquipment().setItemInOffHand(zombie.getEquipment().getItemInOffHand());
        player.addPotionEffects(zombie.getActivePotionEffects());
        player.setFireTicks(zombie.getFireTicks());
        player.setRemainingAir(zombie.getRemainingAir());
        player.teleport(zombie);

        zombie.remove();
    }
    public Set<LinkedDouble> getDoubles() {
        return doubles.getAll();
    }
    public Set<LinkedDouble> getHusks() {
        return husks.getAll();
    }

    public void clearDoubles() {
        doubles.clear();
    }


    public IOpenInv getOpenInv() {
        return openInv;
    }
}
