package net.uomc.plugin.doublelife;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Husk;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.lishid.openinv.internal.ISpecialInventory;

public class DoubleLifeListener implements Listener {
    private DoubleLifePlugin plugin;

    public DoubleLifeListener(DoubleLifePlugin plugin) {
        this.plugin = plugin;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> unAIHusks(), 5L, 5L);
    }

    public void unAIHusks() {
        // TODO clean up this code
        plugin.getHusks().forEach( d -> {
            Mob m;
            Entity e1 = Bukkit.getEntity(d.getUuid1());
            Entity e2 = Bukkit.getEntity(d.getUuid2());
            if (e1 != null && (e1 instanceof Mob)) {
                m = (Mob) e1;
                LivingEntity friend = plugin.getEntityDouble(m);
                if (friend != null && friend instanceof LivingEntity)
                    m.setTarget((LivingEntity) friend);

            }

            if (e2 != null && (e2 instanceof Mob)) {
                m = (Mob) e2;
                LivingEntity friend = plugin.getEntityDouble(m);
                if (friend != null && friend instanceof LivingEntity)
                    m.setTarget((LivingEntity) friend);
            }
        });
    }

    public static double GetMaxHealth(LivingEntity player) {
        return player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
    }

    public void synchroniseHealthAlways(LivingEntity player, LivingEntity dbl) {
        synchroniseHealth(player, dbl, player.getHealth(), player.getAbsorptionAmount());
    }

    public void synchroniseHealth(LivingEntity player, LivingEntity dbl, double newHealth, double newAbsorption) {
        double max = dbl.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        if (dbl.isDead())
            return;

        if (newHealth <= 0.0) {
            dbl.setHealth(0);
        } else if (newHealth > max) {
            dbl.setHealth(max);
            newAbsorption += newHealth - max;
        } else {
            dbl.setHealth(newHealth);
        }

        if (newAbsorption <= 0.0) {
            dbl.setAbsorptionAmount(0);
        } else {
            PotionEffect effect = player.getPotionEffect(PotionEffectType.ABSORPTION);
            if (effect != null)
                dbl.addPotionEffect(
                        new PotionEffect(PotionEffectType.ABSORPTION, effect.getDuration(), effect.getAmplifier()));

            dbl.setAbsorptionAmount(newAbsorption);
        }

    }

    @EventHandler
    public void onRegainHealthEvent(EntityRegainHealthEvent event) {
        if (!(event.getEntity() instanceof LivingEntity))
            return;

        LivingEntity player = (LivingEntity) event.getEntity();

        if (!plugin.hasDouble(player))
            return;

        LivingEntity doublePlayer = plugin.getEntityDouble(player);

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            synchroniseHealthAlways(player, doublePlayer);
        }, 0L);
    }

    @EventHandler
    public void onHuskDamageEvent(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof LivingEntity))
            return;

        LivingEntity damager = (LivingEntity) event.getDamager();
        UUID doublePlayer = plugin.getHuskPlayerId(damager);

        // stop the husk from damaging anything if its a player husk
        if (doublePlayer != null)
            event.setCancelled(true);

        return;
    }

    @EventHandler
    public void onDamageEvent(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof LivingEntity))
            return;

        LivingEntity player = (LivingEntity) event.getEntity();

        LivingEntity notHoldingTotem = null;
        if (!plugin.hasDouble(player))
            return;

        LivingEntity doublePlayer = plugin.getEntityDouble(player);
        if (doublePlayer == null)
            return;

        boolean doublePlayerHoldingTotem = false;
        if (doublePlayer.getEquipment().getItemInMainHand().getType() == Material.TOTEM_OF_UNDYING
                || doublePlayer.getEquipment().getItemInOffHand().getType() == Material.TOTEM_OF_UNDYING) {
            notHoldingTotem = player;
            doublePlayerHoldingTotem = true;
        }

        if (player.getEquipment().getItemInMainHand().getType() == Material.TOTEM_OF_UNDYING
                || player.getEquipment().getItemInOffHand().getType() == Material.TOTEM_OF_UNDYING) {
            notHoldingTotem = doublePlayer;
            doublePlayerHoldingTotem = false;
        }

        if (player.getHealth() - event.getFinalDamage() <= 0 && notHoldingTotem != null) {
            if (doublePlayerHoldingTotem) {
                consumeTotem(doublePlayer);
                event.setCancelled(true);
            }

            totemKill(notHoldingTotem);
        } else {

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                synchroniseHealthAlways(player, doublePlayer);
            }, 0L);
        }
    }

    public void totemKill(LivingEntity player) {
        player.playEffect(EntityEffect.TOTEM_RESURRECT);
        player.playEffect(EntityEffect.HURT);
        player.setHealth(1);
    }

    public void consumeTotem(LivingEntity player) {
        if (player.getEquipment().getItemInMainHand().getType() == Material.TOTEM_OF_UNDYING)
            player.getEquipment().setItemInMainHand(null);

        if (player.getEquipment().getItemInOffHand().getType() == Material.TOTEM_OF_UNDYING)
            player.getEquipment().setItemInOffHand(null);

        totemKill(player);
        player.getActivePotionEffects().forEach(e -> player.removePotionEffect(e.getType()));
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 45 * 20, 1));
        player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 40 * 20, 0));
        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 5 * 20, 1));

    }

    @EventHandler
    public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();
        if (!plugin.hasDouble(player))
            return;

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            LivingEntity dbl = plugin.getEntityDouble(player);
            synchroniseHealthAlways(player, dbl);
        }, 0L);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (!plugin.hasDouble(player))
            return;

        plugin.createHuskDouble(player);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!plugin.hasDouble(player))
            return;

        if (plugin.hasHusk(player))
            plugin.replaceHusk(player);

        // the husk should hold their health, so this isnt necessary
        // LivingEntity dbl = plugin.getEntityDouble(player);
        // synchroniseHealthAlways(dbl, player);
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        if (!plugin.hasDouble(player))
            return;

        LivingEntity dbl = plugin.getEntityDouble(player);
        if (dbl == null)
            return;

        if (dbl.isDead())
            return;

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            synchroniseHealthAlways(dbl, player);
        }, 0L);
    }

    @EventHandler
    public void onHuskDeath(EntityDamageEvent event) throws InstantiationException {
        if (!(event.getEntity() instanceof Husk))
            return;

        LivingEntity entity = (LivingEntity) event.getEntity();

        if (entity.getHealth() - event.getFinalDamage() > 0)
            return;

        UUID playerUuid = plugin.getHuskPlayerId(entity);
        if (playerUuid == null)
            return;

        // this player should always be online
        if (Bukkit.getPlayer(playerUuid) != null)
            return;

        // openinv isnt present, we cant do this
        if (plugin.getOpenInv() == null)
            return;

        Location location = entity.getLocation();
        World world = entity.getWorld();

        Player offlinePlayer = plugin.getOpenInv().loadPlayer(Bukkit.getOfflinePlayer(playerUuid));
        ISpecialInventory inventory = plugin.getOpenInv().getSpecialInventory(offlinePlayer, true);

        Inventory bukkitInventory = inventory.getBukkitInventory();

        if (bukkitInventory == null)
            return;

        Arrays.stream(bukkitInventory.getContents())
                .filter(i -> i != null)
                .forEach(i -> world.dropItemNaturally(location, i));
        inventory.getBukkitInventory().clear();

        plugin.getOpenInv().unload(offlinePlayer);
        ((Husk) entity).getEquipment().clear();
    }
}
