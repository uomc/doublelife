package net.uomc.plugin.doublelife.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import net.uomc.plugin.doublelife.DoubleLifePlugin;

public class ListLinksCommand implements CommandExecutor {

    private DoubleLifePlugin plugin;

    public ListLinksCommand(DoubleLifePlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        plugin.getDoubles().forEach(d -> {
            OfflinePlayer player1 = Bukkit.getOfflinePlayer(d.getUuid1());
            OfflinePlayer player2 = Bukkit.getOfflinePlayer(d.getUuid2());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(ChatColor.BLUE.toString());
            if (player1 != null) {
                stringBuilder.append(player1.getName());
            } else {
                stringBuilder.append("unknown");
            }
            stringBuilder.append(ChatColor.WHITE.toString());
            stringBuilder.append(" <-> ");
            stringBuilder.append(ChatColor.BLUE.toString());
            if (player2 != null) {
                stringBuilder.append(player2.getName());
            } else {
                stringBuilder.append("unknown");
            }
            stringBuilder.append(ChatColor.WHITE.toString());

            sender.sendMessage(stringBuilder.toString());
        });

        return true;
    }
}
