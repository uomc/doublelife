package net.uomc.plugin.doublelife.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.uomc.plugin.doublelife.DoubleLifePlugin;

public class UnLinkCommand implements CommandExecutor {

    private DoubleLifePlugin plugin;

    public UnLinkCommand(DoubleLifePlugin plugin) {
        this.plugin = plugin;
	}

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        if (!sender.isOp())
            return false;

        if (args.length != 1)
            return false;


        String name1 = args[0];
        Player player1 = Bukkit.getPlayer(name1);

        if (player1 == null) {
            sender.sendMessage(ChatColor.DARK_RED + name1 + " doesn't exist");
            return false;
        }

        if (!plugin.hasDouble(player1)) {
            sender.sendMessage(ChatColor.DARK_RED + name1 + " doesnt have a double!");
            return false;
        }

        String doubleName = plugin.getOfflineLinkedDouble(player1).getName();

        plugin.unregisterDouble(player1);
        sender.sendMessage(ChatColor.GRAY + "Successfully ulinked "
                + ChatColor.BLUE + name1
                + ChatColor.WHITE + " from "
                + ChatColor.BLUE + doubleName);

        uncombineHealth(player1);

        Player player2 = Bukkit.getPlayer(doubleName);
        if (player2 != null) {
            uncombineHealth(player2);
        }
        return true;
    }


    public void uncombineHealth(Player player1) {
        double totalhp = player1.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() / 2;
        double hp = player1.getHealth() / 2;
        player1.setHealth(hp);
        player1.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(totalhp);
    }
}
