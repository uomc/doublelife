package net.uomc.plugin.doublelife.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.uomc.plugin.doublelife.DoubleLifePlugin;

public class LinkCommand implements CommandExecutor {

    private DoubleLifePlugin plugin;

    public LinkCommand(DoubleLifePlugin plugin) {
        this.plugin = plugin;
	}

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        if (!sender.isOp())
            return false;

        if (args.length != 2)
            return false;


        String name1 = args[0];
        String name2 = args[1];
        Player player1 = Bukkit.getPlayer(name1);
        Player player2 = Bukkit.getPlayer(name2);

        if (player1 == null) {
            sender.sendMessage(ChatColor.DARK_RED + name1 + " doesn't exist");
            return false;
        }

        if (player2 == null) {
            sender.sendMessage(ChatColor.DARK_RED + name2 + " doesn't exist");
            return false;
        }

        if (plugin.hasDouble(player1)) {
            sender.sendMessage(ChatColor.DARK_RED + name1 + " already has a double");
            return false;
        }


        if (plugin.hasDouble(player2)) {
            sender.sendMessage(ChatColor.DARK_RED + name2 + " already has a double");
            return false;
        }

        plugin.registerDouble(player1, player2);
        sender.sendMessage(ChatColor.GRAY + "Successfully linked "
                + ChatColor.BLUE + name1
                + ChatColor.WHITE + " and "
                + ChatColor.BLUE + name2);

        combineHealth(player1, player2);
        return true;
    }

    /* combine two player's health bars into 1 */
    public static void combineHealth(Player player1, Player player2) {
        double totalhp = player1.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        totalhp += player2.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        double hp = player1.getHealth() + player2.getHealth();
        player1.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(totalhp);
        player2.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(totalhp);
        player1.setHealth(hp);
        player2.setHealth(hp);
    }
}
