package net.uomc.plugin.doublelife;

import java.util.UUID;

public class LinkedDouble {
    private UUID uuid1;
    private UUID uuid2;

    public LinkedDouble(UUID uuid1, UUID uuid2) {
        this.uuid1 = uuid1;
        this.uuid2 = uuid2;
    }

    public boolean has(UUID uuid) {
        if (uuid1.equals(uuid))
            return true;

        if (uuid2.equals(uuid))
            return true;

        return false;
    }

    public UUID get(UUID uuid) {
        if (uuid1.equals(uuid))
            return uuid2;

        if (uuid2.equals(uuid))
            return uuid1;

        return null;
    }

    public UUID getUuid1() {
        return uuid1;
    }

    public UUID getUuid2() {
        return uuid2;
    }
}
