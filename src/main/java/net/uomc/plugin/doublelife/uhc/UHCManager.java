package net.uomc.plugin.doublelife.uhc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Criteria;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.RenderType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import net.md_5.bungee.api.ChatColor;
import net.uomc.plugin.doublelife.DoubleLifePlugin;

public class UHCManager implements Listener {

    private static final double DEFAULT_MAX_HP = 40;
    public static double DEFAULT_WORLD_SIZE = 1000;
    public static double DEFAULT_FINAL_SIZE = 16;
    public static long DEFAULT_DURATION = 60*60;

    private DoubleLifePlugin plugin;

    private boolean started = false;

    public File uhcFile;

    private TeamBiaser biaser;

    public UHCManager(DoubleLifePlugin plugin) {
        this.plugin = plugin;
        plugin.saveDefaultConfig();

        if (!plugin.getConfig().getBoolean("enable_uhc", true))
            return;


        plugin.getCommand("uhc").setExecutor(new UHCCommand(this));
        uhcFile = new File(plugin.getDataFolder(), "uhc.json");
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        if (plugin.getConfig().getBoolean("disable_strength", true))
            Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> removeStrength(), 2L, 2L);

        biaser = new TeamBiaser(plugin);
    }

    public void removeStrength() {
        if (started)
            Bukkit.getOnlinePlayers().forEach(p -> {
                p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
            });
    }

    public boolean isSpectating(Player player) {
        return player.getGameMode() == GameMode.SPECTATOR;
    }

    public void setSpectating(Player player) {
        player.setGameMode(GameMode.SPECTATOR);
        if (plugin.hasDouble(player))
            plugin.unregisterDouble(player);
    }

    public void setNotSpectating(Player player) {
        player.setGameMode(GameMode.ADVENTURE);
    }

    public boolean stopUHC(World world) {
        World nether = Bukkit.getWorld(world.getName() + "_nether");
        World the_end = Bukkit.getWorld(world.getName() + "_the_end");

        world.getWorldBorder().setSize(world.getWorldBorder().getMaxSize());
        world.getWorldBorder().setCenter(0, 0);

        nether.getWorldBorder().setSize(nether.getWorldBorder().getMaxSize());
        nether.getWorldBorder().setCenter(0, 0);

        plugin.clearDoubles();

        List.of(world, nether, the_end).forEach(w -> {
            w.setGameRule(GameRule.DO_MOB_SPAWNING, false);
            w.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            w.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
            w.setTime(0l);
            w.setStorm(false);
            w.setThundering(false);
            w.setDifficulty(Difficulty.PEACEFUL);
        });

        Bukkit.getOnlinePlayers().forEach(player -> {
            player.teleport(world.getSpawnLocation());
            player.setGameMode(GameMode.ADVENTURE);
        });

        started = false;
        return true;
    }

    public boolean startUHC(World world) {
        // if teams creation fails, then don't continue
        if (!createTeams())
            return false;


        World nether = Bukkit.getWorld(world.getName() + "_nether");
        World the_end = Bukkit.getWorld(world.getName() + "_the_end");

        double worldSize = plugin.getConfig().getDouble("world_size", DEFAULT_WORLD_SIZE);
        double finalSize = plugin.getConfig().getDouble("final_size", DEFAULT_FINAL_SIZE);
        long duration = plugin.getConfig().getLong("game_duration", DEFAULT_DURATION);

        world.getWorldBorder().setSize(worldSize);
        world.getWorldBorder().setCenter(0, 0);
        world.getWorldBorder().setSize(finalSize, duration);

        nether.getWorldBorder().setSize(worldSize / 8);
        nether.getWorldBorder().setCenter(0, 0);
        nether.getWorldBorder().setSize(finalSize / 8, duration);

        spreadPlayers(world, worldSize);

        List.of(world, nether, the_end).forEach(w -> {
            w.setGameRule(GameRule.DO_INSOMNIA, false);
            w.setGameRule(GameRule.NATURAL_REGENERATION, false);
            w.setGameRule(GameRule.SPECTATORS_GENERATE_CHUNKS, false);
            w.setGameRule(GameRule.DO_MOB_SPAWNING, true);
            w.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, true);
            w.setGameRule(GameRule.DO_WEATHER_CYCLE, true);
            w.setDifficulty(Difficulty.NORMAL);
            w.setTime(0l);
            w.setStorm(false);
            w.setThundering(false);
        });

        Scoreboard sb = Bukkit.getScoreboardManager().getMainScoreboard();
        if (sb.getObjective("Health") == null) {
            Objective objective = sb.registerNewObjective("Health", Criteria.HEALTH, "Health", RenderType.HEARTS);
            objective.setDisplaySlot(DisplaySlot.PLAYER_LIST);
        }


        started = true;
        return true;
    }

    public boolean createTeams() {
        List<Player> playersList = new ArrayList<Player>(Bukkit.getOnlinePlayers());
        playersList.removeIf(player -> isSpectating(player));

        // if we don't have an even number of players, then we can't do this
        if (playersList.size() % 2 != 0)
            return false;

        playersList.forEach(p -> {
            if (plugin.hasDouble(p))
                plugin.unregisterDouble(p);
        });

        // todo sort playersList based on something
        Collections.shuffle(playersList);
        Collections.sort(playersList, (x, y) -> biaser.getWeighting(x.getName()) - biaser.getWeighting(y.getName()));

        Player player1, player2;
        while (playersList.size() > 0) {
            player1 = playersList.get(0);
            playersList.remove(0);

            player2 = playersList.get(playersList.size()-1);
            playersList.remove(playersList.size()-1);

            linkPlayers(player1, player2);
        }

        plugin.saveAll();
        return true;
    }

    private void linkPlayers(Player player1, Player player2) {
        double maxHP = plugin.getConfig().getDouble("max_hp", DEFAULT_MAX_HP);

        plugin.registerDouble(player1, player2);

        player1.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxHP);
        player1.setHealth(maxHP);

        player2.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxHP);
        player2.setHealth(maxHP);
    }

    public void spreadPlayers(World world, double worldSize) {
        plugin.getDoubles().stream().forEach(d -> {
            Player player1 = Bukkit.getPlayer(d.getUuid1());
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                    "spreadplayers 0 0 "
                    + 40
                    + " "
                    + (worldSize*0.4)
                    + " false "
                    + player1.getName());
        });


            plugin.getDoubles().forEach(d -> {
                Player player1 = Bukkit.getPlayer(d.getUuid1());
                Player player2 = Bukkit.getPlayer(d.getUuid2());


                Bukkit.getScheduler().runTaskLater(plugin, () -> {
                        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                                "spreadplayers "
                                + player1.getLocation().getX()
                                + " "
                                + player1.getLocation().getZ()
                                + " 16 16 false "
                                + player2.getName());
                        Bukkit.getScheduler().runTaskLater(plugin, () -> {
                            initializePlayer(player1);
                            initializePlayer(player2);
                        }, 0L);
                    }, 0L);
                });
                // spread player2 somewhere near player 1
    }

    public void initializePlayer(Player player) {
        player.getInventory().clear();
        player.setFireTicks(0);
        player.setLevel(0);
        player.setFallDistance(0);
        player.getActivePotionEffects().clear();

        player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20*60, 255));
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20*60*5, 255));
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*15, 255));
        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20*10, 255));
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 20*10, 255));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*10, 255));

        player.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 0.75f, 1.0f);
        player.sendMessage(ChatColor.WHITE + "You are being teleported... ");

        Bukkit.getScheduler().runTaskLater(plugin, () -> {

            player.sendTitle("", "Your soulmate is...", 20, 3*20, 20);
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0f, 1.5f);

                Player soulmate = plugin.getPlayerDouble(player);
                player.sendTitle(ChatColor.GREEN + soulmate.getName(), "", 0, 20*3, 20);
                player.setGameMode(GameMode.SURVIVAL);

                player.sendMessage(ChatColor.WHITE + "You are linked with " + ChatColor.BLUE + soulmate.getName());
                player.sendMessage(ChatColor.WHITE + "If you take damage, they will take damage");
                player.sendMessage(ChatColor.WHITE + "Try to be the last pair standing..." + ChatColor.GREEN + "good luck!");
            }, 20L*5L);
        }, 20L*4L);
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (!started)
            return;

        Player player = event.getEntity();
        player.setGameMode(GameMode.SPECTATOR);
        if (plugin.hasDouble(player))
            plugin.unregisterDouble(player);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (started)
            return;

        if (!(event.getEntity() instanceof Player))
            return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerConsume(PlayerInteractEvent event) {
        if (!plugin.getConfig().getBoolean("disable_stew", true))
            return;

        if (!started)
            return;

        if (event.getItem() == null)
            return;

        if (event.getItem().getType() == Material.SUSPICIOUS_STEW)
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!started)
            return;

        Player player = event.getPlayer();
        if (!plugin.hasDouble(player))
            player.setGameMode(GameMode.SPECTATOR);
    }

    public void saveAll() throws IOException {
        JsonObject treeJSON = new JsonObject();
        treeJSON.addProperty("started", started);

        uhcFile.getParentFile().mkdirs();
        FileWriter fw = new FileWriter(uhcFile);
        try {
            fw.write(treeJSON.toString());
        } finally {
                fw.flush();
                fw.close();
        }
    }

    public void loadAll() throws FileNotFoundException {
        if (!uhcFile.exists())
            return;

        FileReader reader = new FileReader(uhcFile);
        JsonReader jsonReader = new JsonReader(reader);
        JsonElement tree = JsonParser.parseReader(jsonReader);
        JsonObject treeJSON = tree.getAsJsonObject();

        started = treeJSON.get("started").getAsBoolean();
    }

}
