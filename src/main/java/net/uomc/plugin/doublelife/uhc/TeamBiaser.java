package net.uomc.plugin.doublelife.uhc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.uomc.plugin.doublelife.DoubleLifePlugin;

public class TeamBiaser {
    private File scoresFile;
    private Map<String, Integer> weights;
    private int average = 0;

    public TeamBiaser(DoubleLifePlugin plugin) {
        scoresFile = new File(plugin.getDataFolder(), "weights.yml");
        weights = new HashMap<String, Integer>();

        initialLoad();
    }

    public int getWeighting(String playername) {
        if (weights.containsKey(playername)) {
            return weights.get(playername);
        }
        return average;
    }

    public void initialLoad() {
        YamlConfiguration yamlConfiguration = new YamlConfiguration();
        if (!scoresFile.exists())
            return;

        try {
            yamlConfiguration.load(scoresFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        if (!yamlConfiguration.contains("weights"))
            return;

        yamlConfiguration.getConfigurationSection("weights").getKeys(false)
                .forEach(key -> {
                    Integer content = (int) yamlConfiguration.get("weights." + key);
                    weights.put(key, content);
                });

        average = getMedian(new ArrayList<Integer>(weights.values()));
    }

    public static int getMedian(List<Integer> numArray) {
        Collections.sort(numArray);

        if (numArray.size() % 2 == 0)
            return (int) (numArray.get(numArray.size() / 2) + numArray.get(numArray.size() / 2 - 1)) / 2;
        else
            return numArray.get(numArray.size() / 2);
    }
}
