package net.uomc.plugin.doublelife.uhc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UHCCommand implements CommandExecutor {
    public UHCManager uhc;

    public UHCCommand(UHCManager uhc) {
        this.uhc = uhc;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        if (!sender.isOp())
            return false;

        if (args.length != 1)
            return false;

        if ("stop".equals(args[0]))
            return uhc.stopUHC(((Player) sender).getWorld());

        if ("start".equals(args[0]))
            return uhc.startUHC(((Player) sender).getWorld());

        return false;
    }
}
