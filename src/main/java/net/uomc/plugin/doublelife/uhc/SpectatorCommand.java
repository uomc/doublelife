package net.uomc.plugin.doublelife.uhc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpectatorCommand implements CommandExecutor {
    public UHCManager uhc;

    public SpectatorCommand(UHCManager uhc) {
        this.uhc = uhc;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        if (!sender.isOp())
            return false;

        if (args.length != 1)
            return false;

        String name = args[0];
        Player player = Bukkit.getPlayer(name);

        if (uhc.isSpectating(player)) {
            uhc.setSpectating(player);
            sender.sendMessage(ChatColor.BLUE + player.getName() + ChatColor.WHITE + " is now a spectator");
        } else {
            uhc.setNotSpectating(player);
            sender.sendMessage(ChatColor.BLUE + player.getName() + ChatColor.WHITE + " is no longer a spectator");
        }

        return true;
    }
}
